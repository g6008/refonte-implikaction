
import http from 'k6/http';
import { check} from 'k6';

// export const BACKEND_URL = "http://localhost:8080";
export const BACKEND_URL = __ENV.BACKEND_URL;

export function getUser(){
    const url = `${BACKEND_URL}/api/auth/login`;
    const payload = JSON.stringify({
      password: "password",
        username: "matthieu"
    });
    const params = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const result = http.post(url, payload, params);
    if(result.status !== 200) 
      check(null, {
        'API POST /api/auth/login': (resp) => false,  
      });
    return result;
}

export function getRandomNumber(min, max) {  
  return Math.floor(
    Math.random() * (max - min + 1) + min
  )
}
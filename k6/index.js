
import http from 'k6/http';
import { check} from 'k6';

import { getUser, BACKEND_URL, getRandomNumber } from './utils.js'

export const options = { 
  // stages: [
  //   { duration: "1m", target: 5 }, 
  //   { duration: "1m", target: 10 }, 
  //   { duration: "1m", target: 20 }, 
  // ],
  iterations:50,
  vus:5,
  thresholds: {
    http_req_duration: ['p(99)<2000'], // 99% of requests must complete below 2s
  },
};

export default function() {
  const response = getUser();
  const user = JSON.parse(response.body);
  const params = { 
    headers: {
      'Authorization': `Bearer ${user.authenticationToken}`,
      'X-XSRF-TOKEN': response.cookies['XSRF-TOKEN'][0].value,
      'Content-Type': 'application/json',
    },   
  };

  function shouldGetAllCommunity() {
    try{
      const url = `${BACKEND_URL}/api/users/community`; 
      const result = http.get(url, params);
      check(result, {
        'API GET /api/users/community': (result) => result.status === 200,  
      });   
    }catch(err){
      check(null, {
        'API GET /api/users/community': (resp) => false,  
      });
    }
  };

  function shouldGetAllFriends() {
    try{
      const url = `${BACKEND_URL}/api/users/${user.currentUser.id}/friends`; 
      const result = http.get(url, params);
      check(result, {
        'API GET /api/users/{userId}/friends': (result) => result.status === 200,  
      });
    }catch(err){
      check(null, {
        'API GET /api/users/{userId}/friends': (resp) => false,  
      });
    }
  };

  function shouldGetSentFriendRequest() {
    try{
      const url = `${BACKEND_URL}/api/users/friends/sent`; 
      const result = http.get(url, params);
      check(result, {
        'API GET /api/users/friends/sent': (result) => result.status === 200,  
      });
    }catch(err){
      check(null, {
        'API GET /api/users/friends/sent': (resp) => false,  
      });
    }
  };

  function shouldGetReceivedFriendRequest() {
    try{
      const url = `${BACKEND_URL}/api/users/friends/received`; 
      const result = http.get(url, params);
      check(result, {
        'API GET /api/users/friends/received': (result) => result.status === 200,  
      });
    }catch(err){
      check(null, {
        'API GET /api/users/friends/received': (resp) => false,  
      });
    }
  };

  function shouldSentRequestRelation() {
    try{
      const url = `${BACKEND_URL}/api/relations/request/${getRandomNumber(1, 100)}`;      
      const result = http.post(url, null, params);  
      check(result, {
        'API POST /api/relations/request/{receiverId}': (result) => result.status < 500,  
      });
    }catch(err){
      check(null, {
        'API POST /api/relations/request/{receiverId}': (resp) => false,  
      });
    }
  };

  function shouldCancelRelationByUser() {
    try{
      const url = `${BACKEND_URL}/api/relations/${getRandomNumber(1, 100)}/cancel`; 
      const result = http.del(url, null, params);
      check(result, {
        'API DELETE /api/relations/{userId}/cancel': (result) => result.status < 500,  
      });
    }catch(err){
      check(null, {
        'API DELETE /api/relations/{userId}/cancel': (resp) => false,  
      });
    }
  };
  
  function shouldDeleteRelationBySender() {
    try{
      const url = `${BACKEND_URL}/api/relations/${getRandomNumber(1, 100)}/decline`; 
      const result = http.del(url, null, params);
      check(result, {
        'API DELETE /api/relations/{senderId}/decline': (result) => result.status < 500,  
      });
    }catch(err){
      check(null, {
        'API DELETE /api/relations/{senderId}/decline': (resp) => false,  
      });
    }
  };

  function shouldConfirmRelation() {
    try{
      const url = `${BACKEND_URL}/api/relations/${getRandomNumber(1, 100)}/confirm`; 
      const result = http.get(url, params);
      check(result, {
        'API GET /api/relations/{senderId}/confirm': (result) => result.status < 500,  
      });
    }catch(err){
      check(null, {
        'API GET /api/relations/{senderId}/confirm': (resp) => false,  
      });
    }
  };

  function shouldGetJobById() {
    try{
      const url = `${BACKEND_URL}/api/job-postings/${getRandomNumber(1, 200)}`; 
      const result = http.get(url, params);
      check(result, {
        'API GET /api/job-postings/{jobId}': (result) => result.status < 500,  
      });
    }catch(err){
      check(null, {
        'API GET /api/job-postings/{jobId}': (resp) => false,  
      });
    }
  };

  function shouldDeleteJobById() {
    try{
      const url = `${BACKEND_URL}/api/job-postings/${getRandomNumber(1, 500)}`; 
      const result = http.del(url, null, params);
      check(result, {
        'API DELETE /api/job-postings': (result) => result.status < 500,  
      });
    }catch(err){
      check(null, {
        'API DELETE /api/job-postings': (resp) => false,  
      });
    }
  };

  function shouldCreateOrUpdateJob() {
    try{
      const url = `${BACKEND_URL}/api/job-postings`; 
      const payload = JSON.stringify({     
        id: getRandomNumber(0, 200),   
        keywords:"",
        location:"Saint-Denis",
        salary:"2000",
        title:"Offre d'alternance dev react",
        shortDescription:"<p>blabla</p>",
        description:"<p>blabl</p>",
        contractType:"CDI",
        businessSector:"ASSURANCE",
        company: {
          id:155,
          name:"Amazon",
          logo:"https://www.sportbuzzbusiness.fr/wp-content/uploads/2016/04/amazon-fr.png",
          description:"<p>Amazon &nbsp;est une entreprise de commerce en ligne américaine basée à Seattle. Elle est l'un des géants du Web, regroupés sous l'acronyme GAFAM10, aux côtés de Google, Apple, Facebook et Microsoft. Créée par Jeff Bezos en juillet 1994, l'entreprise a été introduite en bourse au NASDAQ en mai 1997.</p>",
          url:"https://www.amazon.fr/"
        }
      });
      const result = http.post(url, payload, params);
      check(result, {
        'API POST /api/job-postings': (result) => result.status === 200,  
      });
    }catch(err){
      check(null, {
        'API POST /api/job-postings': (resp) => false,  
      });
    }
  };

  function shouldValidateJob() {
    try{
      const url = `${BACKEND_URL}/api/job-postings/${getRandomNumber(0, 200)}/validate`; 
      const result = http.patch(url, null, params);
      check(result, {
        'API PATCH /api/job-postings/{jobId}/validate': (result) => result.status < 500,  
      });
    }catch(err){
      check(null, {
        'API PATCH /api/job-postings/{jobId}/validate': (resp) => false,  
      });
    }
  };

  function shoulGetAllAppliesByUserId() {
    try{
      const url = `${BACKEND_URL}/api/applies`; 
      const result = http.get(url, params);
      check(result, {
        'API GET /api/applies': (result) => result.status === 200,  
      });
    }catch(err){
      check(null, {
        'API GET /api/applies': (resp) => false,  
      });
    }
  };

  function shouldCreateOrUpdateCompagny() {
    try{
      const url = `${BACKEND_URL}/api/companies`; 
      const payload = JSON.stringify({    
        description: "blabla",
        id: getRandomNumber(0, 200),
        logo: "string",
        name: "test compagny k6",
        url: "string"    
      });
      const result = http.post(url, payload, params);
      check(result, {
        'API POST /api/companies': (result) => result.status === 200,  
      });
    }catch(err){
      check(null, {
        'API POST /api/companies': (resp) => false,  
      });
    }
  };

  function shouldGetAllCompagnies() {
    try{
      const url = `${BACKEND_URL}/api/companies`; 
      const result = http.get(url, params);
      check(result, {
        'API GET /api/companies': (result) => result.status === 200,  
      });

    }catch(err){
      check(null, {
        'API GET /api/companies': (resp) => false,  
      });
    }
  };

  shouldGetAllCommunity();
  shouldGetAllFriends();
  shouldGetSentFriendRequest();
  shouldGetReceivedFriendRequest();

  shouldSentRequestRelation();
  shouldCancelRelationByUser();
  shouldDeleteRelationBySender();
  shouldConfirmRelation();

  shouldCreateOrUpdateJob();
  shouldGetJobById();
  shouldDeleteJobById();
  shouldValidateJob();
  
  shouldCreateOrUpdateCompagny();
  shouldGetAllCompagnies();

  shoulGetAllAppliesByUserId();
}




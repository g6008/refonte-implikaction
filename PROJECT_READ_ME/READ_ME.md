# READ ME Compte-rendu du projet Quality

Le projet Quality a été réalisé dans le cadre du cours, l'objectif aura été à partir d'un projet existant incorporer différents types de tests et créer un build en sortie.
***
## Sommaire

**Partie Gitlab :**

- Registration des gitlab-runner
- Initialisation des differents stages
- Creation de variables protégées

**Partie Sonar :**

- Recherche Sonar
- Configuration du runner et du scanner.
- Mise en place d'un dashboard Sonar

**Partie Build et Release :**

- Création des dockerfiles
- Configuration des runners pour le front, le back et la db
- Release de l'image dans le container registry

**Partie Deploy :**

- Mise en prod d'un serveur
- Configuration du serveur et du runner

**Partie Test :**

- Recherche sur les différents outils de test
- Création des différents tests (K6 et Cypress)
- Configuration des tests et des runners pour les tests (UnitTest, K6 et Cypress)
- Mise en place des différents dashboards (Sonar, K6 et Cypress)

***

## Membres du groupe

**AHAMADA Mohamed-Zaïd**

**FADJO Crepin Hugues**

**CHEN Yu**

**IOAN Simon**

***
## Lien utile
Lien du projet initial : https://github.com/dyno-nuggets/refonte-implicaction/

Clone du projet sur gitlab : https://gitlab.com/g6008/refonte-implikaction.git

SonarCloud Review : https://sonarcloud.io/organizations/g6008/projects

Cypress Dashboard : https://dashboard.cypress.io/projects/dnzksf/runs

Cypress Dashboard Invitation Link : https://dashboard.cypress.io/invitation/a5c4bd19-28e5-4dac-9214-918e11c09ed4

K6 Dashboard : https://app.k6.io/projects/3577461

K6 Invitation Link : (On peut l'envoyer que par mail)

Container registry : https://gitlab.com/g6008/refonte-implikaction/container_registry

Adresse du front sur DigitalOcean : http://164.92.74.227:4200/

Adresse du back sur DigitalOcean : http://164.92.74.227:8080/

## Launching project
Pour lancer le projet :

`npm i`

`docker-compose up`

`mvn spring-boot:run -Dspring-boot.run.profiles=local`

`ng serve` (dans frontend-implicaction)

Pour simplifier la marche à suivre nous avons ajouter un makefile dans le dossier du projet.

Nous avons alors remplacé les commandes précédentes par les commandes suivantes :
 - `make start`
 - `make build`
 - `make up`

## Gitlab

Nous avons donc créé un fichier .gitlab-ci.yml qui contient les différentes tâches que nous avons définies pour le projet.
Voici donc nos parties Unit_test, Build, Deploy et Test_ui_load.

![Gitlab CI](data/gitlab-runner_run.png)

Elles représentent les différentes tâches que nous avons définies pour le projet.

`gitlab-runner install`

`sudo gitlab-runner register -n --url https://gitlab.com/ --registration-token REGISTRATION_TOKEN --executor docker --description "My Docker Runner" --docker-image "docker:19.03.12" --docker-privileged --tag-list "implik-docker,implik-aws" --run-untagged="true" --locked="false"`

`sudo gitlab-runner register -n --url https://gitlab.com/ --registration-token REGISTRATION_TOKEN --executor shell --description "My Shell Runner" --tag-list "implik-shell,implik-bash" --run-untagged="true" --locked="false"`

`gitlab-runner start`

Nous avons donc nos runners custom et nous avons dans notre .gitlab-ci.yml ajouté des tags aux differentes parties susceptible d'avoir des conflits pour permettre à notre runner d'avoir la priorité sur les runners partagés.
Nous avons également ajouté quelque règles pour le lancement de runner dans les différentes parties.

![Gitlab Runner Specific](data/gitlab-runner_specific.png)

Nous avons par la suite ajouté quelques variables d'environnements.

![Gitlab Price](data/gitlab-runner_price.png)

** Free community edition **

***
## Phase Sonar

### Depart avec SonarQube
 Création du docker-compose pour lancer un serveur Sonar → Paramétrage et execution des tests
 (Le lancement fonctionnait que sur windows pour permettre à tout le groupe d'en profiter on a changé de plateforme)

### Passage sur SonarCloud
Cette solution propose également un dashboard accessible facilement.
Initialisation des var d'environnement, paramétrage, …
Failure dans l'exécution des tests par le runner SonarCloud :
https://www.testcontainers.org/supported_docker_environment/continuous_integration/gitlab_ci/

Pour run les tests de la librairie Test containers, il faut passer par un docker auparavant on utilisait une image Docker. 

**(Juste après avoir verifier le code nous building le projet pour préparer les prochains tests)**

Notre Runner s'occupe donc de faire la verification du code.

**Vous pouvez retrouver le compte rendu Sonar dans un dashboard sur le lien au début du fichier.**

###  SonarCloud
![Sonar Cloud](data/sonarcloud_price.png)

**Free for opensource project**
###  SonarQube
![Sonar Qube](data/sonarqube_price.png)

**Free community edition**

***
## Phase Build et Release

La première phase aura été de dockerisé le front, qui ne l'étais pas dans le projet initial.
Par la suite, nous avons créé nos fichiers et configurés nos différents runner pour pouvoir obtenir un build utilisable par le serveur.
Ces images ont ensuite été stockées dans les conteneurs registries proposées par Gitlab.
Nous avons donc notre 3 tâches dans la partie Build pour les builds.
![img.png](data/gitlab-runner_container.png)

***
## Phase Deploy

Nous avons donc pris un serveur digital ocean le temps du projet.
Grâce à la phase de build, nous avons recupérer un build, qu'on va alors utilisé pour le déploiement sur le serveur.
Par la suite nous avons configuré le runner pour le déploiement et puisque nous avons le back, le front et la db de dockerisé la phase de deploy a été plus simple.

![Digital Ocean Price](data/digital-ocean_price.png)

Dans notre cas nous sommes parti sur un Droplet.
***
## Phase Test de unitaires

Les tests unitaires sont lancés via le runner en parralèle au Sonar avant tout les autres parties.
Il était deja présent dans le code initial on a seulement de l'ajouter dans le runner execution de nos tests unitaires.
Il ne fallait juste pas oublié que la librairie Test containers était utilisée et donc il fallait passer par du docker in docker.

![unit-test](data/unit-test.png)

***
## Phase Test de charge

Pour K6 nous avons commencé par faire les tests en local en installant l'environnement dans un docker.
Dans le dossier principal si votre back est lancé faite s'il n'est pas deja faite :

`docker-compose up`

![K6 Run](data/k6_run.png)

Nous avons commencé à creuser le lancement depuis le cloud et l'utilisation du dashboard k6, on a dû l'arrêter suite à un incident.

![K6 Dashboard](data/k6_dashboard.png)

Par la suite on a commencé à mettre dans notre runner le lancement de nos tests de charge.

## K6
![K6](data/k6_price.png)

**50 cloud tests for Free**

***
## Phase Test de l'interface

Après l'installation de cypress et la création des différents tests, on a pu faire nos tests.

Nous les avons premièrement lancé en local puis mis sur le dashboard en ligne,
Sur le frontend :

`ng run frontend-implicaction:cypress-open`

`ng run frontend-implicaction:cypress-run --record --key KEY`

![Cypress Dashboard](data/cypress_dashboard.png)

Actuellement dans notre runner le lancement de nos tests se fait uniquement sur une plateforme.
Nous avons commencé par faire les tests cypress sur la machine en prod puis sur celle build précédemment par le sonar.

**Comme pour les tests precedent notre runner s'occupe de faire les tests cypress.**
**Vous pouvez retrouver le compte rendu Cypress dans un dashboard sur le lien au début du fichier.**

### Cypress
![Cypress](data/cypress_price.png)

**500 test result for Free**





.DEFAULT_GOAL := help	
# Show this help message
# Multiple lines are supported too
# When it's first, both commands works:
#  - `make`
#  - `make help`
help: 
	@cat $(MAKEFILE_LIST) | docker run --rm -i xanders/make-help | more

################################# ENV VARIABLE #################################
APP_NAME = api
FOLDER_NAME = refonte-implikaction

args1 = $(filter-out $@,$(wordlist 2,2,$(MAKECMDGOALS)))
args2 = $(filter-out $@,$(wordlist 3,3,$(MAKECMDGOALS)))
args3 = $(filter-out $@,$(wordlist 4,4,$(MAKECMDGOALS)))
args="$(wordlist 2,99,$(MAKECMDGOALS))" # List all args max 99
%:
	@grep -oE '^$(wordlist 1,1,$(MAKECMDGOALS)):([^=]|$$)' Makefile > /dev/null

################################ BASI COMMANDE #################################
# Classic start-up process.
start: pull build up swagger browser logs

# Builds, (re)creates, starts, and attaches to containers for a service.
#  - `ARGUMENT1` = Name target container, Default target all container
up:
	docker-compose up -d $(if ${args1},${args1},)

# Stops containers and removes containers, networks, volumes, and images created by up.
down:
	docker-compose down

# Stream in you terminal the logs in format json for the target container.
#  - `ARGUMENT1` = Name target container, Default  target container is this app
logs: 
	docker-compose logs -f $(if ${args1},${args1},${APP_NAME})

# Print in you terminal the logs in format json for the target container. 
#  - `ARGUMENT1` = Name target container, Default  target container is this app
log:
	sudo cat `docker inspect --format='{{.LogPath}}' ${FOLDER_NAME}_$(if ${args1},${args1},${APP_NAME})_1`

# Clear container logs for the target container. 
#  - `ARGUMENT1` = Name target container, Default  target container is this app
logs-clear: 
	sudo sh -c "truncate -s 0 $$(docker inspect --format='{{.LogPath}}' ${FOLDER_NAME}_$(if ${args1},${args1},${APP_NAME})_1)"
	make logs

# Restart the container for the target container. 
#  - `ARGUMENT1` = Name target container, Default target all container
restart:
	docker-compose restart $(if ${args1},${args1},)

# Building all image
#  - `ARGUMENT1` = Name target container, Default target all container
build: 
	docker-compose build $(if ${args1},${args1},)

# Pull all image
#  - `ARGUMENT1` = Name target container, Default target all container
pull: 
	docker-compose pull $(if ${args1},${args1},)

# Open swagger
swagger:
	xdg-open "http://localhost:8080/swagger-ui.html"

# Open site web
browser:
	xdg-open  "http://localhost:4200/"

# Open adminer
adminer:
	xdg-open  "http://localhost:9030/?server=db&username=root&db=implicaction"

# Start terminal bash in a container
#  - `ARGUMENT1` = Name container. Default, this app container name
bash:
	docker-compose exec $(if ${args1},${args1},${APP_NAME}) bash
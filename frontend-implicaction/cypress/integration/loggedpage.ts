describe('Login test', () => {
  it(' Should Check login button is present', () => {
    cy.visit('/');
    cy.get('body').find('#cy-signin').should('be.visible');
  });

  it('Should Check login button is clickable', () => {
    cy.get('body').find('#cy-signin').click();
    cy.url().should('include', '/login');
  });

  // should log in with valid credentials
  it('Should login user', () => {
    signIn('admin', 'password');
    cy.wait(2000);
    cy.get('body').find('#dropdownUser2').should('be.visible');
  });
});


describe('Go to all page', () => {
  it('Should Visits the Home page', () => {
    visitPage('Accueil', '#cy_header_0' , '/');
  });

  it('Should Visits Entreprise page', () => {
    visitPage('Espace entreprise', '#cy_header_1', '/entreprise');
  });

  it('Should Visits Community page', () => {
    visitPage('Communauté', '#cy_header_2', 'users');
  });

  it('Should Visits Jobs page', () => {
    visitPage('Offres d\'emploi', '#cy_header_3', 'jobs');
  });

  it('Should Visits Forum page', () => {
    visitPage('Forum', '#cy_header_4', 'forum');
  });

});

// sign in function
function signIn(username, password): void{
  cy.get('body').find('#floatingUsername').clear().type(username);
  cy.get('body').find('#floatingPassword').clear().type(password);
  cy.get('body').find('#cy-login-button-submit').click();
}

// visit page function
function visitPage(contains, buttonId, urlCheck): void{
  cy.contains(contains).click();
  cy.get('body').find(buttonId).click();
  cy.url().should('include', urlCheck);
}

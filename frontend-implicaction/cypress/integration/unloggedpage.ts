describe('Go to Home page', () => {
  it('Should Visits the Home page', () => {
    cy.visit('/');
    cy.contains('Accueil').click();
    cy.url().should('include', '');
  });

  it('Should Visit Espace Entreprise Page', () => {
    cy.visit('/');
    cy.contains('Espace entreprise').click();
    cy.url().should('include', '/entreprise');
  });

});


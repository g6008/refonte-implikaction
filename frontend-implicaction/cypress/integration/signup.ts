const result = randomString();

describe('Signup test', () => {
  it('Should Check signup button is present', () => {
    cy.visit('/');
    cy.get('body').find('#cy-signup').should('be.visible');
  });
  it('Should Check signup button is clickable', () => {
    cy.get('body').find('#cy-signup').click();
    cy.url().should('include', '/sign-up');
  });

  it('Should register a already exist user', () => {
    cy.get('body').find('#cy-signup').click();
    cy.url().should('include', '/sign-up');
    signUp('admin', 'test', 'test', 'admin', 'admin', 'admin@test.fr');
    // cy.url().should('include', '/login');
    cy.get('body').find('.alert-danger').contains('Un compte utilisateur existe déjà avec ce nom d\'utilisateur.');
  });

  it('Should register a new user', () => {

    cy.get('body').find('#cy-signup').click();
    cy.url().should('include', '/sign-up');
    signUpTest();
    // cy.url().should('include', '/login');
    cy.get('body').find('.alert-success').contains('Votre inscription a bien été enregistrée. Elle doit maintenant être validée par un administrateur.');
  });

});

describe('Login test', () => {
  it(' Should Check login button is present', () => {
    cy.get('body').find('#cy-signin').should('be.visible');
  });

  it('Should Check login button is clickable', () => {
    cy.get('body').find('#cy-signin').click();
    cy.url().should('include', '/login');
  });
  // should log in with invalid credentials
  it('Should log in with invalid credentials', () => {
    signIn('test' + randomString(), 'test' + randomString());
    cy.get('body').find('.alert-danger').contains('Nom d\'utilisateur ou mot de passe incorrect.');
  });

  // should log in with valid credentials but not validated
  it('Should log in with valid credentials but not validated', () => {
    signIn(result + 'test', 'test');
    cy.get('body').find('.alert-danger').contains('Votre compte n\'a pas encore été activé.');

  });
});

// sign in function
function signIn(username, password): void{
  cy.get('body').find('#floatingUsername').clear().type(username);
  cy.get('body').find('#floatingPassword').clear().type(password);
  cy.get('body').find('#cy-login-button-submit').click();
}

// sign up function
function signUp(username, password, confirmPassword, firstname, lastname, email): void{
  cy.get('body').find('#floatingUsername').clear().type(username);
  cy.get('body').find('#floatingEmail').clear().type(email);
  cy.get('body').find('#floatingPassword').clear().type(password);
  cy.get('body').find('#floatingConfirmPassword').clear().type(confirmPassword);
  cy.get('body').find('#floatingFirstname').clear().type(firstname);
  cy.get('body').find('#floatingLastname').clear().type(lastname);
  cy.get('body').find('#floatingSubmit').click();
}

function signUpTest(): void{
  cy.get('body').find('#floatingUsername').clear().type(result + 'test');
  cy.get('body').find('#floatingEmail').clear().type(result + 'test@test.fr');
  cy.get('body').find('#floatingPassword').clear().type('test');
  cy.get('body').find('#floatingConfirmPassword').clear().type('test');
  cy.get('body').find('#floatingFirstname').clear().type('test');
  cy.get('body').find('#floatingLastname').clear().type('test');
  cy.get('body').find('#floatingSubmit').click();
}

// visit page function
function visitPage(contains, buttonId, urlCheck): void{
  cy.contains(contains);
  cy.get('body').find(buttonId).click();
  cy.url().should('include', urlCheck);
}


function randomString(): string {
  let resultat = '';
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (let i = 0; i < 15; i++) {
    resultat += characters.charAt(Math.floor(Math.random() * characters.length));
  }
  return resultat;
}
